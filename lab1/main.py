import pydicom
import numpy as np
import os
import matplotlib.pyplot as plt
from skimage.filters import threshold_local, threshold_otsu
from PIL import Image

PATIENT = "0de72529c30fe642bc60dcb75c87f6bd"
INPUT_FOLDER = "CT_chest_scans/" + PATIENT + "/"
MODE = "otsu"
MODE_LOCAL_OR_NOT = {"local_mean": True, "local_median": True, "otsu": False}
OUTPUT_FOLDER = "output/" + PATIENT + "_Segmented_with_" + MODE + "_threshold/"

def load_scan(path):
    slices = [pydicom.dcmread(path + s) for s in os.listdir(path)]
    slices.sort(key = lambda x: float(x.ImagePositionPatient[2]))
        
    return slices

def remove_pixel_padding(slices):	
	for ds in slices:
		ds.PixelPaddingValue = 0

	return slices

def get_pixels_hu(slices, normalize=True):
    image = np.stack([s.pixel_array for s in slices])
    # Convert to int16 (from sometimes int16), 
    # should be possible as values should always be low enough (<32k)
    image = image.astype(np.int16)

    # Set outside-of-scan pixels to 0
    # The intercept is usually -1024, so air is approximately 0
    image[image == -2000] = 0

    # Convert to Hounsfield units (HU)
    for slice_number in range(len(slices)):
        
        intercept = slices[slice_number].RescaleIntercept
        slope = slices[slice_number].RescaleSlope
        
        if slope != 1:
            image[slice_number] = slope * image[slice_number].astype(np.float64)
            image[slice_number] = image[slice_number].astype(np.int16)
            
        image[slice_number] += np.int16(intercept)

    if normalize:
    	image += 1024 # we want the minimum HU = 0, see https://www.materialise.com/en/faq/what-are-hounsfield-units-hu
    	image = image / 4096.0
    	return np.array(image)
    
    return np.array(image, dtype=np.int16)

def print_statistics(slices, hu_slices):
	first_slice = slices[0]
	first_hu_slice = hu_slices[0]

	first_slice_copy = np.array(first_slice)
	print(first_slice_copy)
	print("-----raw data-----")
	print("max: {}".format(first_slice.pixel_array.max()))
	print("min: {}".format(first_slice.pixel_array.min()))
	print("mean: {}".format(first_slice.pixel_array.mean()))
	print("std: {}".format(first_slice.pixel_array.std()))
	print("-----Hounsfield units-----")	
	print("max: {}".format(first_hu_slice.max()))
	print("min: {}".format(first_hu_slice.min()))
	print("mean: {}".format(first_hu_slice.mean()))
	print("std: {}".format(first_hu_slice.std()))

def plot(slices):
	plt.figure()
	for i in range(1,26):
	    plt.subplot(5,5,i)
	    plt.imshow(slices[i-1], cmap ='gray')
	    plt.xticks([])
	    plt.yticks([])
	plt.show()

def compute_threshold(image, mode):
	if mode == "local_mean":
		return threshold_local(image, block_size=21, method="mean")
	elif mode == "local_median":
		return threshold_local(image, block_size=21, method="median")
	elif mode == "otsu":
		return threshold_otsu(image)

def segment(image, mode, plot=True):
	thresh = compute_threshold(image, mode)
	is_local = MODE_LOCAL_OR_NOT[mode]
	binary = image >= thresh

	if plot:
		fig, axes = plt.subplots(ncols=3, figsize=(8, 5))
		ax = axes.ravel()
		ax[0] = plt.subplot(1, 3, 1)
		ax[1] = plt.subplot(1, 3, 2)
		ax[2] = plt.subplot(1, 3, 3)

		ax[0].hist(image.ravel(), bins=80)
		ax[0].set_title('Histogram')
		if is_local:
			ax[0].axvline(thresh.mean(), color='r')
		else:
			ax[0].axvline(thresh, color='r')

		ax[1].imshow(image, cmap=plt.cm.gray)
		ax[1].set_title('Original')
		ax[1].axis('off')

		ax[2].imshow(binary, cmap=plt.cm.gray)
		ax[2].set_title('Thresholded')
		ax[2].axis('off')

		plt.show()

	return binary

def store_for_3d_viewer(first_patient_hu, MODE):
	if not os.path.isdir("output"):
		os.mkdir("output")

	if not os.path.isdir(OUTPUT_FOLDER):
		os.mkdir(OUTPUT_FOLDER)

	for index, per_slice in enumerate(first_patient_hu):
		binary = segment(per_slice, MODE, plot=False)
		binary = np.array(binary * 255, dtype = np.uint8)
		im = Image.fromarray(binary)
		im.save(OUTPUT_FOLDER + str(index) + ".png")

first_patient = load_scan(INPUT_FOLDER)
first_patient = remove_pixel_padding(first_patient)
first_patient_hu = get_pixels_hu(first_patient, normalize=True)
print_statistics(first_patient, first_patient_hu)
plot(first_patient_hu)
# 80 can be arbitrary number within the len of first_patient_hu
segment(first_patient_hu[80], MODE)
store_for_3d_viewer(first_patient_hu, MODE)