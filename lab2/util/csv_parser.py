# modify from https://github.com/winst13/cs231n-mammogram/blob/master/scripts/label_data.py

import csv
import numpy as np
import os
from os.path import join

data_dir = "CBIS-DDSM_Dataset"
file_name = "000001.dcm"  # cuz I will only use cropped image file

def get_csvreader_from_filepath(path):
    csvfile = open(path, newline='') # We do not scope the open() call, want to REMAIN open!
    reader = csv.DictReader(csvfile, fieldnames=None) # Infer fieldnames from first row
    return reader

def get_label(row):
    def is_malignant(string): # Based on labels in csv. There are multiple classes of labels.
        return "MALIGNANT" in string
    def is_benign(string): # Based on labels in csv
        return "BENIGN" in string
    label_field = row["pathology"]
    if is_malignant(label_field) and not is_benign(label_field):
        return 1
    elif is_benign(label_field) and not is_malignant(label_field):
        return 0
    else:
        raise ValueError("Unrecognized field, cannot tell if benign (0) or mal. (1)")

def construct_sample_name(row, prefix):
    """ param |row|: An OrderedDict representing one row in the csv.
        string tuples of (fieldname, content).
    """
    patient_id = row["patient_id"]
    assert len(patient_id) == 7 and patient_id[:2] == 'P_'
    LR = row["left or right breast"]
    assert LR in ("LEFT", "RIGHT")
    view = row["image view"]
    assert view in ("MLO", "CC")
    abnormality_id = row["abnormality id"]
    sample_name = '_'.join([prefix, patient_id, LR, view, abnormality_id])
    # e.g. "Calc-Test_P_00038_LEFT_MLO_1"

    # print("Found row in csv with sample name:", sample_name)
    return sample_name

def construct_sample_path(sample_name):
    onion = join(data_dir, sample_name)
    for root, dirs, files in os.walk(onion):
        for _dir in dirs:
            onion = join(onion, _dir)

    sample_path = join(onion, file_name)
    # e.g. "Calc-Test_P_00038_LEFT_MLO_1/.../.../00001.dcm"

    return sample_path

def build_sample_to_class_map(reader, prefix):
    sample_to_class = {}
    for row in reader:
        sample_name = construct_sample_name(row, prefix)
        class_label = get_label(row)  # int: 0 or 1

        LR = row["left or right breast"]
        view = row["image view"]
        # Collect all labels for the tumor(s) in that mammogram
        sample_path = construct_sample_path(sample_name)
        sample_to_class[sample_path] = [class_label, LR, view]
    
    return sample_to_class

def check_data_distribution(labeled_samples):
    malignant_label_num = 0
    benign_label_num = 0
    for sample_name, labels in labeled_samples.items():
        if labels[0] == 1:
            malignant_label_num += 1
        else:
            benign_label_num += 1

    print("MALIGNANT num: %d, BENIGN num: %d" % (malignant_label_num, benign_label_num))
    return (malignant_label_num, benign_label_num)

# return: key = real dicom path, value = list which len is 3 (contain ground truth label, left or right breast tag and image view tag)
def build_mapping(csv_path, prefix):
    print("Reading csv from:", csv_path)
    reader = get_csvreader_from_filepath(csv_path)
    labeled_samples = build_sample_to_class_map(reader, prefix)
    print("Built mapping to labels for %d samples in csv." % len(labeled_samples))
    check_data_distribution(labeled_samples)
    return labeled_samples


#Examples of how to use:

# csv_path = "csv_labels/calc_case_description_train_set.csv"
# csv_path = "csv_labels/calc_case_description_test_set.csv"

# if __name__ == "__main__":
#     print("Working with:", csv_path)
#     build_mapping(csv_path)
    
#     print("****************")
#     print("*     Done!    *")
#     print("****************")