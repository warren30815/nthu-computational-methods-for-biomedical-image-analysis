import torch
from torch.utils.data import Dataset, DataLoader
from torch.utils.data import sampler
import torchvision.datasets as dset
import torchvision.transforms as T
import numpy as np
import os
import PIL
import pydicom as dicom
import random
import skimage
# from imblearn.under_sampling import RandomUnderSampler

from util.csv_parser import build_mapping, check_data_distribution

NUM_CLASSES = 2
IMAGE_SIZE = (1024,1024)

class MammogramDataset(Dataset):
    def __init__(self, csv_path, prefix, transform = None, need_balance_dataset=False):
        self.csv_path = csv_path
        self.transform = transform

        print("Working with:", csv_path)
        # key = real dicom path, value = list which len is 3 (contain ground truth label, left or right breast tag and image view tag)
        tmp_dicom_path_and_labels_dict = build_mapping(csv_path, prefix)
        self.dicom_paths = []
        self.dicom_path_and_labels_dict = {}
        malignant_label_num, benign_label_num = check_data_distribution(tmp_dicom_path_and_labels_dict)

        if not need_balance_dataset:
            self.dicom_path_and_labels_dict = {k: v for k,v in tmp_dicom_path_and_labels_dict.items()}
            for key, value in self.dicom_path_and_labels_dict.items():
                self.dicom_paths.append(key)
        else:
            # region: balance positive and negative sample
            self.dicom_path_and_labels_dict = {k: v for k,v in tmp_dicom_path_and_labels_dict.items() if v[0] == 1}
            current_negative_count = 0

            for key, value in list(tmp_dicom_path_and_labels_dict.items()):
                if value[0] == 0:
                    current_negative_count += 1
                    if current_negative_count <= malignant_label_num:
                        self.dicom_path_and_labels_dict[key] = value
                    else:
                        break
            # endregion
            for key, value in self.dicom_path_and_labels_dict.items():
                self.dicom_paths.append(key)
            random.shuffle(self.dicom_paths)
            print("After balance dataset")
            check_data_distribution(self.dicom_path_and_labels_dict)

    def __len__(self):
        return len(self.dicom_paths)

    def __getitem__(self, idx):
        dicom_path = self.dicom_paths[idx]
        label = self.dicom_path_and_labels_dict[dicom_path][0]
        ds = dicom.dcmread(dicom_path)
        image = ds.pixel_array
        image = skimage.transform.resize(image, IMAGE_SIZE)
        image = np.expand_dims(image, axis=0)

        if self.transform:
            image = self.transform(image)
        # image = np.squeeze(image, 0)
        
            
        sample = {'image': image, 'label': int(label)}
        return sample