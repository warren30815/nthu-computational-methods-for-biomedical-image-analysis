# modify from https://github.com/winst13/cs231n-mammogram/blob/master/main.py

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, sampler

import torchvision
import torchvision.datasets as dset
import torchvision.transforms as T
from torch.nn import functional as F

import numpy as np
import argparse
import PIL

# from imblearn.under_sampling import RandomUnderSampler
from util.util import print
from util.dataset_class import MammogramDataset
from util.checkpoint import save_model, load_model
from util.metrics import *
from model.baseline_model import BaselineModel
from model.mammogram_densenet import MammogramDenseNet
from model.helper import *

# args
debug = False
augment = False
USE_GPU = True
learning_rate = 1e-3
dropout = 0.2
l2reg = 0
VAL_RATIO = 0.2  # validation set ratio
save_every = 1
exp_name = "baseline" # specify the name of the experiment
model_name = "baseline"
mode = 'train'
load_best = True  # if we want to load an existing model
no_val_list = True
train_csv_path = "csv_labels/calc_case_description_train_set.csv"
test_csv_path = "csv_labels/calc_case_description_test_set.csv"
train_prefix = "Calc-Training"
test_prefix = "Calc-Test"

#Hyperparameters
BATCH_SIZE = 4
EPOCH = 10

# CONSTANTS
# IMAGE_SIZE = 2048*2048

'''
Take a loader, model, and optimizer.  Use the optimizer to update the model
based on the training data, which is from the loader.  Does not terminate,
saves best checkpoint and latest checkpoint
'''
def train(loader_train, loader_val, model, optimizer, EPOCH, loss_list = [], val_acc_list = []):
    model = model.to(device=device)
    epoch = 0
    while epoch < EPOCH:
        tot_correct = 0.0
        tot_samples = 0.0
        tot_loss = 0.0
        for t, sample in enumerate(loader_train):
            x = sample['image']
            y = sample['label']
            # Move the data to the proper device (GPU or CPU)
            x = x.to(device=device, dtype=dtype)
            y = y.to(device=device, dtype=torch.float)

            scores = model(x).view(-1)
            # print("Predicted scores:", scores)
            optimizer.zero_grad()
            loss = F.binary_cross_entropy(scores, y)
            loss.backward()
            optimizer.step()
            loss_list.append(loss.item()) ## Fixed. Maybe this line was the memory leak
            
            #training acc, precision, recall, etc. metrics
            num_samples = scores.size(0)
            preds = scores > 0.5
            truepos, falsepos, trueneg, falseneg = evaluate_metrics(preds, y)
            assert (truepos + falsepos + trueneg + falseneg) == num_samples
            num_correct = truepos + trueneg
            tot_correct += num_correct
            tot_samples += num_samples
            tot_loss += loss.item()
                
        print("Validation: ")
        val_acc = check_accuracy(loader_val, model)
        val_acc_list.append(val_acc)
        train_acc = float(tot_correct)/tot_samples
        if epoch % save_every == 0:
            save_model({
                'epoch': epoch,
                'state_dict': model.state_dict(),
                'optimizer' : optimizer.state_dict(),
                'loss_list' : loss_list,
                'val_acc_list': val_acc_list
                }, val_acc, exp_name)
        print ("EPOCH %d, val accuracy = %06f"%(epoch + 1, float(val_acc)))
        print ("train accuracy = %06f, loss = %06f"%(train_acc, tot_loss))
        '''
        for name, param in model.named_parameters():
            if param.requires_grad:
                print (name, param.data)
        '''
        epoch += 1
        
'''
Takes a data loader and a model, then returns the model's accuracy on
the data loader's data set
'''
def check_accuracy(loader, model, cutoff = 0.5):
    tot_correct, tot_samples = 0, 0
    tot_truepos, tot_falsepos, tot_trueneg, tot_falseneg = 0, 0, 0, 0
    model.eval()  # set model to evaluation mode
    with torch.no_grad():
        for sample in loader:
            x = sample['image']
            y = sample['label']
            x = x.to(device=device, dtype=dtype)  # move to device, e.g. GPU
            y = y.to(device=device, dtype=torch.long)
            scores = model(x).view(-1)
            tot_samples += scores.size(0)
            preds = scores > 0.5
            truepos, falsepos, trueneg, falseneg = evaluate_metrics(preds, y)
            tot_truepos += truepos
            tot_falsepos += falsepos
            tot_trueneg += trueneg
            tot_falseneg += falseneg
    print ("tp = %d, fp = %d, tn = %d, fn = %d, tot = %d"%(tot_truepos, tot_falsepos, tot_trueneg, tot_falseneg, tot_samples))
    assert (tot_truepos + tot_falsepos + tot_trueneg + tot_falseneg) == tot_samples
    f1 = get_f_beta(tot_truepos, tot_falsepos, tot_trueneg, tot_falseneg)
    f2 = get_f_beta(tot_truepos, tot_falsepos, tot_trueneg, tot_falseneg, beta=2)
    print ("f1 score = %06f"%(f1))
    print ("f2 score = %06f"%(f2))
    precision = get_precision(tot_truepos, tot_falsepos, tot_trueneg, tot_falseneg)
    recall = get_recall(tot_truepos, tot_falsepos, tot_trueneg, tot_falseneg)
    print ("precision = %06f, recall = %06f"%(precision, recall))
    tot_correct += tot_truepos + tot_trueneg
    acc = float(tot_correct)/tot_samples
    '''
    for name, param in model.named_parameters():
        if param.requires_grad:
            print (name, param.data)
    '''
    return acc

if __name__ == "__main__":
    dtype = torch.float32
    if USE_GPU and torch.cuda.is_available(): #Determine whether or not to use GPU
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    print('using device:', device)

    # region: loading data and preprocess

    # augment_transform = T.Compose([
    #                 T.ToPILImage(),
    #                 #RandomResizedCrop gives memory leaks, following two lines replaces it
    #                 #T.RandomResizedCrop(IMAGE_SIZE, scale=(0.9, 1.0), interpolation=PIL.Image.BICUBIC),
    #                 T.Resize(int(1024/0.9) , interpolation=PIL.Image.BICUBIC),
    #                 T.RandomCrop(1024),
    #                 T.RandomHorizontalFlip(),
    #                 T.ToTensor()
    #             ])

    # normal_transform = T.Compose([
    #                 T.ToPILImage(),
    #                 T.Resize((1024,1024) , interpolation=PIL.Image.BICUBIC),
    #                 T.ToTensor()
    #             ])

    if mode == 'train':
        # if augment:
        #     train_data = MammogramDataset(train_csv_path, train_prefix, transform=augment_transform)
        # else:
        #     train_data = MammogramDataset(train_csv_path, train_prefix)
        train_data = MammogramDataset(train_csv_path, train_prefix, need_balance_dataset=True)

        NUM_VAL = int(len(train_data)*VAL_RATIO) 
        NUM_TRAIN = len(train_data) - NUM_VAL 
        loader_train = DataLoader(train_data, batch_size=BATCH_SIZE, sampler=sampler.SubsetRandomSampler(range(NUM_TRAIN)), drop_last=True)
        loader_val = DataLoader(train_data, batch_size=BATCH_SIZE, sampler=sampler.SubsetRandomSampler(range(NUM_TRAIN, NUM_TRAIN + NUM_VAL)), drop_last=True)
    elif mode == 'tiny':
        loader_tiny_train = DataLoader(train_data, batch_size=BATCH_SIZE, sampler=sampler.SubsetRandomSampler(range(101)), drop_last=True)
        loader_tiny_val = DataLoader(train_data, batch_size=BATCH_SIZE, sampler=sampler.SubsetRandomSampler(range(100, 200)), drop_last=True)    
    elif mode == 'test':
        test_data = MammogramDataset(test_csv_path, test_prefix, need_balance_dataset=False)
        NUM_TEST = len(test_data)
        loader_test = DataLoader(test_data, batch_size=BATCH_SIZE, drop_last=True)

    # endregion

    betas = (0.9, 0.999)

    if model_name == "baseline":
        model = BaselineModel(drop_rate=dropout)
    print (model)

    if device == torch.device('cuda'):
        model.cuda()

    optimizer = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()), lr=learning_rate, betas = betas, weight_decay=l2reg)
    # optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.9)

    loss_list = []
    val_acc_list = []

    if mode == 'train':
        train(loader_train, loader_val, model, optimizer, EPOCH, loss_list = loss_list, val_acc_list = val_acc_list)
    elif mode == 'tiny':
        train(loader_tiny_train, loader_tiny_val, model, optimizer, EPOCH, loss_list = loss_list, val_acc_list = val_acc_list)
    elif mode == 'test':
        if load_best:
            if no_val_list:
                epoch, loss_list, _ = load_model(exp_name, model, optimizer, mode = 'best')
            else:
                epoch, loss_list, val_acc_list = load_model(exp_name, model, optimizer, mode = 'best')

        acc = check_accuracy(loader_test, model)
        save_plot(loss_list, epoch, exp_name, "train_loss")
        if not no_val_list:
            save_plot(val_acc_list, epoch, exp_name, "val_acc")
        print ("accuracy = %06f"%(acc))