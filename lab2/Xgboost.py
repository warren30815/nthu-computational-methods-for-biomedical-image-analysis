from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.utils.data import DataLoader, sampler
import numpy as np
import torchvision
from torchvision import datasets, models, transforms
import matplotlib.pyplot as plt
import time
import os
import copy

from util.dataset_class import MammogramDataset
import xgboost as xgb
from sklearn.metrics import accuracy_score, confusion_matrix, precision_score, recall_score

# mode = "train"

# args
train_csv_path = "csv_labels/calc_case_description_train_set.csv"
test_csv_path = "csv_labels/calc_case_description_test_set.csv"
train_prefix = "Calc-Training"
test_prefix = "Calc-Test"
VAL_RATIO = 0.2

#Hyperparameters
BATCH_SIZE = 4

# Data augmentation and normalization for training
# Just normalization for validation
transform = transforms.Compose([
                transforms.Lambda(lambda x: np.repeat(x, 3, axis=0)),
                transforms.Lambda(lambda x: np.transpose(x, (1, 2, 0))),
                transforms.ToTensor(),
                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
                ])

train_data = MammogramDataset(train_csv_path, train_prefix, transform, need_balance_dataset=True)
# NUM_VAL = int(len(train_data)*VAL_RATIO) 
# NUM_TRAIN = len(train_data) - NUM_VAL
NUM_TRAIN = len(train_data) 
loader_train = DataLoader(train_data, batch_size=BATCH_SIZE, sampler=sampler.SubsetRandomSampler(range(NUM_TRAIN)), drop_last=True)
# loader_val = DataLoader(train_data, batch_size=BATCH_SIZE, sampler=sampler.SubsetRandomSampler(range(NUM_TRAIN, NUM_TRAIN + NUM_VAL)))
test_data = MammogramDataset(test_csv_path, test_prefix, transform, need_balance_dataset=False)
loader_test = DataLoader(test_data, batch_size=BATCH_SIZE, drop_last=True)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

pretrained_model = torchvision.models.googlenet(pretrained=True)
# remove some layer
pretrained_model = nn.Sequential(*list(pretrained_model.children())[:-1])
# pretrained_model = nn.Sequential(
#     pretrained_model,
#     nn.AdaptiveAvgPool2d((8,8))
# )
for param in pretrained_model.parameters():
    param.requires_grad = False
# Set model to evaluation mode
pretrained_model.eval()

if device == torch.device('cuda'):
    pretrained_model.cuda()

# # model summary
# for layer, child in enumerate(pretrained_model.children()):
#     print(layer)
#     print(child)
print ("GoogleNet + Xgboost model")

# extract train data feature 
train_data_features = []  # list of torch tensor
train_data_labels = []
print("extract train data feature")
for sample in loader_train:
    x = sample['image']
    labels = sample['label']
    x = x.type(torch.FloatTensor)
    features = pretrained_model(x)
    # print(features.shape)
    features = features.view(BATCH_SIZE, -1)
    for feature in features:
        # print(feature.shape)
        train_data_features.append(feature)  
    for label in labels:
        train_data_labels.append(label)

train_data_features = torch.stack(train_data_features)
train_data_labels = torch.stack(train_data_labels)
train_data_features = train_data_features.numpy()
train_data_labels = train_data_labels.numpy()
xgb_model = xgb.XGBClassifier(objective="binary:logistic", random_state=42)
xgb_model.fit(train_data_features, train_data_labels)

# extract test data feature 
test_data_features = []  # list of torch tensor
test_data_labels = []
print("extract test data feature")
for sample in loader_test:
    x = sample['image']
    labels = sample['label']
    x = x.type(torch.FloatTensor)
    features = pretrained_model(x)
    features = features.view(BATCH_SIZE, -1)
    for feature in features:
        test_data_features.append(feature)  
    for label in labels:
        test_data_labels.append(label)

test_data_features = torch.stack(test_data_features)
test_data_labels = torch.stack(test_data_labels)
test_data_features = test_data_features.numpy()
test_data_labels = test_data_labels.numpy()
y_pred = xgb_model.predict(test_data_features)

print("accuracy_score: ")
print(accuracy_score(test_data_labels, y_pred))
print("confusion_matrix: ")
print(confusion_matrix(test_data_labels, y_pred))
print("precision_score: ")
print(precision_score(test_data_labels, y_pred))
print("recall_score: ")
print(recall_score(test_data_labels, y_pred))

